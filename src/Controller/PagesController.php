<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Controller\Controller;
use Cake\Event\Event;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;
use Cake\Mailer\Email;

class PagesController extends AppController
{

    public function index(){
        
        $this->viewBuilder()->layout('front-end');
        
    }
    
    public function contact(){
        
        if ($this->request->is('post') || $this->request->is('put')):
            $FrontendContactTable = TableRegistry::get('FrontendContact');                        
            $availability = $FrontendContactTable->newEntity();
            $frontendDetail = $FrontendContactTable->patchEntity($availability, $this->request->data);
            if ($FrontendContactTable->save($frontendDetail)) {
                
                $email = new Email();
                $email->from(["info@sesniel.com" => 'WebBuilder'])
                    ->to("deloso.sesniel@yahoo.com")
//                    ->bcc($emailInfo[5])
                    ->subject($this->request->data['name'] . " Contacted you!")
                    ->send($this->request->data['message']);
                return $this->redirect($this->referer()); 
            }
        endif;
        
    }
    
    
}
