<?php
namespace App\Controller\Manage;

use App\Controller\AppController;

use Cake\Controller\Controller;
use Cake\Event\Event;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;
use Cake\Mailer\Email;


class BuildController extends AppController
{
    
    public function layout() {
        
        $this->checkLoginStatus();
//        $this->viewBuilder()->layout('combine');
        
    }
    
    public function lists(){
        
        $this->viewBuilder()->layout('frontend');
        $pagesTable = TableRegistry::get('Pages');
        $pageDetails= $pagesTable->find('all',['conditions' => [
            'Pages.user_id' => $this->request->session()->read("id")
        ]])->order(['created' => 'desc']);
        $this->set(compact('pageDetails'));
        
    }
    
    public function upload(){
        
        $imagesTable = TableRegistry::get('Images');
        $dir = $this->checkFolder($this->request->session()->read("token"));      
        
	$allowed_types = array("image/jpeg", "image/gif", "image/png", "image/svg", "application/pdf");


	/* DON'T CHANGE ANYTHING HERE!! */

	$return = array();

	if ( !isset($_FILES['imageFileField']['error']) || is_array($_FILES['imageFileField']['error']) ) {

            $return['code'] = 0;
            $return['response'] = $_FILES['imageFileField']['error'];

	}

        
	$name = $this->checkFilename($_FILES['imageFileField']['name']);
	$file_type = $_FILES['imageFileField']['type'];
        
        $imageSave = DS . "webroot".DS."images".DS."users".DS.$this->request->session()->read("token");

        $imageData = $imagesTable->newEntity();
        $imageData->user_id  = $this->request->session()->read("id");
        $imageData->file_name = $name;
        $imageData->name      = $_FILES['imageFileField']['name'];
        $imagesTable->save($imageData);

	if(in_array($file_type, $allowed_types)) {

		if (move_uploaded_file( $_FILES['imageFileField']['tmp_name'], $dir.DS.$name ) ) {

                    $return['code'] = 1;
                    $return['response'] = $imageSave."/".$name;


		} else {

			$return['code'] = 0;
			$return['response'] = "The uploaded file couldn't be saved. Please make sure you have provided a correct upload folder and that the upload folder is writable.";

		}

		//print_r ($_FILES);

	} else {

		$return['code'] = 0;
		$return['response'] = "File type not allowed";

	}

        $this->set(compact('return'));    

        
    }    
    
    public function skeleton(){
        
        $this->viewBuilder()->layout('skeleton');
        
    }

    public function save() {
                
        $this->viewBuilder()->layout('blank');      
        if( $this->request->data['data'] ) {
            $pagesTable = TableRegistry::get('Pages');
//            debug(json_encode($this->request->data)); exit;
        
        if($this->request->session()->check("id")):
            $this->changePagesStats($this->request->session()->read("id"));
        endif;
        
        foreach($this->request->data['data']['pages'] as $key => $page):
            $pageData = $pagesTable->newEntity();
            if($this->request->session()->read("id")):
                $pageData['user_id'] = $this->request->session()->read("id");
            else:
                $pageData['user_id'] = 1;
            endif;
            $pageData['status'] = 'active';
            $pageData['name'] = $key;
            $pageData['code'] = json_encode($page['blocks']);
            $pagesTable->save($pageData);
        endforeach;
            
    }
            
        $return['responseCode'] = 1;
        $return['responseHTML'] = '<h5>Hooray!</h5> <p>The site was saved successfully!</p>';       
        $this->set(compact('return'));     
        
    }
    
    public function siteJson(){
        
        $this->loadComponent('RequestHandler');    
        
        $this->RequestHandler->renderAs($this, 'json');
        $this->response->type('application/json');
    
        $pagesTable = TableRegistry::get('Pages');
        
        if($this->request->session()->check("id")):
            $pagesDetails = $pagesTable->find('all', ['conditions' => [
                'Pages.user_id' => $this->request->session()->read("id"),
                'Pages.status' => 'active'    
            ]]);
        else:
            $pagesDetails = $pagesTable->find('all', ['conditions' => [
                'Pages.id' => 50    
            ]]);
        endif;
        
//$pages['responsiveMode'] = 'tablet';

    foreach($pagesDetails as $page): 
    
            $pages[$page->name]['blocks'] = json_decode($page->code);
        
        
    endforeach;

        
        $this->set('pages', $pages); 
            
    }
    
    private function changePagesStats($userId){
        
        $pagesTable = TableRegistry::get('Pages');
        $pagesDetails = $pagesTable->find('all', ['conditions' => [
            'Pages.user_id' => $userId,
            'Pages.status' => 'active'          
        ]]);
        foreach($pagesDetails as $page):
            $pageUpdate = $pagesTable->get($page->id);
            $pageUpdate['status'] = "closed";
            $pagesTable->save($pageUpdate);
        endforeach;
            
                    
    }
    
    public function get($id){
        
        $pagesTable = TableRegistry::get('Pages');
        $pageDetails = $pagesTable->get($id);
        return $pageDetails;
        
    }
    
    public function display($id){
        
        $this->viewBuilder()->layout('combine');
        $data = $this->get($id);
//        echo debug($data);
        echo $data->code;
        exit;
        $this->set(compact('data'));
        
    }
    
    private function checkFilename($imageName){
        
        $imgNameCheck = md5(mt_rand()) . "-" . $imageName;
        $imagesTable = TableRegistry::get('Images');
        $imgDetails = $imagesTable->find('all',['conditions' => [
            'Images.user_id' => $this->request->session()->read("id"),
            'Images.file_name' => $imgNameCheck
        ]])->first();
        do{
            $imgNameCheck = md5(mt_rand()) . "-" . $imageName;
        }while(!empty($imgDetails));
        return $imgNameCheck;
        
    }
    
}
