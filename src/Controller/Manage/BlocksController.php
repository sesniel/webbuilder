<?php
namespace App\Controller\Manage;
use App\Controller\AppController;
use Cake\Controller\Controller;
use Cake\Event\Event;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;
use Cake\Mailer\Email;


class BlocksController extends AppController
{
    
    public function design($type = "", $block = "", $template = "default"){
        
        $blocksTable = TableRegistry::get('Blocks');
        $blockDetails= $blocksTable->find('all', ['conditions' => [
            'Blocks.count' => $block,
            'Blocks.type' => $type
        ]])->first();
        $this->set(compact('blockDetails'));
        $this->viewBuilder()->layout('skeleton');
        
    }
        
}
