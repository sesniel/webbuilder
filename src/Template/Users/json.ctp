{"elements": {
    "Navigation":[
    	{"url":"webroot/elements/navigation1.html","height":50, "thumbnail":"/webroot/elements/images/thumbs/navigation1.jpg"}
    ],
	"Intros":[
    	{"url":"webroot/elements/intro1.html","height":710, "thumbnail":"/webroot/elements/images/thumbs/intro1.jpg"}
    ]
    }
}

<?php
// {"elements": {
//     "Navigation":[
//         {"url":"/webroot/elements/navigation1.html","height":50, "thumbnail":"/webroot/elements/images/thumbs/navigation1.jpg"},
//         {"url":"/webroot/elements/navigation2.html","height":71, "thumbnail":"/webroot/elements/images/thumbs/navigation2.jpg"},
//         {"url":"/webroot/elements/navigation3.html","height":70, "thumbnail":"/webroot/elements/images/thumbs/navigation3.jpg"},
//         {"url":"/webroot/elements/navigation4.html","height":111, "thumbnail":"/webroot/elements/images/thumbs/navigation4.jpg"},
//         {"url":"/webroot/elements/navigation5.html","height":41, "thumbnail":"/webroot/elements/images/thumbs/navigation5.jpg"},
//         {"url":"/webroot/elements/navigation6.html","height":70, "thumbnail":"/webroot/elements/images/thumbs/navigation6.jpg"},
//         {"url":"/webroot/elements/navigation7.html","height":72, "thumbnail":"/webroot/elements/images/thumbs/navigation7.jpg"},
//         {"url":"/webroot/elements/navigation8.html","height":42, "thumbnail":"/webroot/elements/images/thumbs/navigation8.jpg"},
//         {"url":"/webroot/elements/navigation9.html","height":41, "thumbnail":"/webroot/elements/images/thumbs/navigation9.jpg"},
//         {"url":"/webroot/elements/navigation10.html","height":109, "thumbnail":"/webroot/elements/images/thumbs/navigation10.jpg"},
//         {"url":"/webroot/elements/navigation11.html","height":69, "thumbnail":"/webroot/elements/images/thumbs/navigation11.jpg"},
//         {"url":"/webroot/elements/navigation12.html","height":220, "thumbnail":"/webroot/elements/images/thumbs/navigation12.jpg"},
//         {"url":"/webroot/elements/navigation13.html","height":213, "thumbnail":"/webroot/elements/images/thumbs/navigation13.jpg"}
//     ],
//     "Intros":[
//         {"url":"/webroot/elements/intro1.html","height":710, "thumbnail":"/webroot/elements/images/thumbs/intro1.jpg"},
//         {"url":"/webroot/elements/intro2.html","height":880, "thumbnail":"/webroot/elements/images/thumbs/intro2.jpg"},
//         {"url":"/webroot/elements/intro3.html","height":611, "thumbnail":"/webroot/elements/images/thumbs/intro3.jpg"},
//         {"url":"/webroot/elements/intro4.html","height":633, "thumbnail":"/webroot/elements/images/thumbs/intro4.jpg"},
//         {"url":"/webroot/elements/intro5.html","height":772, "thumbnail":"/webroot/elements/images/thumbs/intro5.jpg"},
//         {"url":"/webroot/elements/intro6.html","height":723, "thumbnail":"/webroot/elements/images/thumbs/intro6.jpg"},
//         {"url":"/webroot/elements/intro7.html","height":647, "thumbnail":"/webroot/elements/images/thumbs/intro7.jpg"},
//         {"url":"/webroot/elements/intro8.html","height":566, "thumbnail":"/webroot/elements/images/thumbs/intro8.jpg"},
//         {"url":"/webroot/elements/intro9.html","height":674, "thumbnail":"/webroot/elements/images/thumbs/intro9.jpg"},
//         {"url":"/webroot/elements/intro10.html","height":881, "thumbnail":"/webroot/elements/images/thumbs/intro10.jpg"},
//         {"url":"/webroot/elements/intro11.html","height":736, "thumbnail":"/webroot/elements/images/thumbs/intro11.jpg"},
//         {"url":"/webroot/elements/intro12.html","height":594, "thumbnail":"/webroot/elements/images/thumbs/intro12.jpg"},
//         {"url":"/webroot/elements/intro13.html","height":599, "thumbnail":"/webroot/elements/images/thumbs/intro13.jpg"},
//         {"url":"/webroot/elements/intro14.html","height":740, "thumbnail":"/webroot/elements/images/thumbs/intro14.jpg"},
//         {"url":"/webroot/elements/intro15.html","height":800, "thumbnail":"/webroot/elements/images/thumbs/intro15.jpg"},
//         {"url":"/webroot/elements/intro16.html","height":494, "thumbnail":"/webroot/elements/images/thumbs/intro16.jpg"},
//         {"url":"/webroot/elements/intro17.html","height":708, "thumbnail":"/webroot/elements/images/thumbs/intro17.jpg"},
//         {"url":"/webroot/elements/intro18.html","height":524, "thumbnail":"/webroot/elements/images/thumbs/intro18.jpg"},
//         {"url":"/webroot/elements/intro19.html","height":817, "thumbnail":"/webroot/elements/images/thumbs/intro19.jpg"},
//         {"url":"/webroot/elements/intro20.html","height":522, "thumbnail":"/webroot/elements/images/thumbs/intro20.jpg"}
//     ],
//     "Headers":[
//         {"url":"/webroot/elements/header1.html","height":701, "thumbnail":"/webroot/elements/images/thumbs/header1.jpg"},
//         {"url":"/webroot/elements/header2.html","height":670, "thumbnail":"/webroot/elements/images/thumbs/header2.jpg"},
//         {"url":"/webroot/elements/header3.html","height":866, "thumbnail":"/webroot/elements/images/thumbs/header3.jpg"},
//         {"url":"/webroot/elements/header4.html","height":781, "thumbnail":"/webroot/elements/images/thumbs/header4.jpg"},
//         {"url":"/webroot/elements/header5.html","height":910, "thumbnail":"/webroot/elements/images/thumbs/header5.jpg"},
//         {"url":"/webroot/elements/header6.html","height":712, "thumbnail":"/webroot/elements/images/thumbs/header6.jpg"},
//         {"url":"/webroot/elements/header7.html","height":672, "thumbnail":"/webroot/elements/images/thumbs/header7.jpg"},
//         {"url":"/webroot/elements/header8.html","height":686, "thumbnail":"/webroot/elements/images/thumbs/header8.jpg"},
//         {"url":"/webroot/elements/header9.html","height":866, "thumbnail":"/webroot/elements/images/thumbs/header9.jpg"},
//         {"url":"/webroot/elements/header10.html","height":720, "thumbnail":"/webroot/elements/images/thumbs/header10.jpg"},
//         {"url":"/webroot/elements/header11.html","height":659, "thumbnail":"/webroot/elements/images/thumbs/header11.jpg"},
//         {"url":"/webroot/elements/header12.html","height":736, "thumbnail":"/webroot/elements/images/thumbs/header12.jpg"},
//         {"url":"/webroot/elements/header13.html","height":670, "thumbnail":"/webroot/elements/images/thumbs/header13.jpg"}
//     ],
//     "Content Blocks":[
//         {"url":"/webroot/elements/content1.html","height":300, "thumbnail":"/webroot/elements/images/thumbs/content1.jpg"},
//         {"url":"/webroot/elements/content2.html","height":210, "thumbnail":"/webroot/elements/images/thumbs/content2.jpg"},
//         {"url":"/webroot/elements/content3.html","height":210, "thumbnail":"/webroot/elements/images/thumbs/content3.jpg"},
//         {"url":"/webroot/elements/content4.html","height":405, "thumbnail":"/webroot/elements/images/thumbs/content4.jpg"},
//         {"url":"/webroot/elements/content5.html","height":500, "thumbnail":"/webroot/elements/images/thumbs/content5.jpg"},
//         {"url":"/webroot/elements/content6.html","height":249, "thumbnail":"/webroot/elements/images/thumbs/content6.jpg"},
//         {"url":"/webroot/elements/content7.html","height":249, "thumbnail":"/webroot/elements/images/thumbs/content7.jpg"},
//         {"url":"/webroot/elements/content8.html","height":339, "thumbnail":"/webroot/elements/images/thumbs/content8.jpg"},
//         {"url":"/webroot/elements/content9.html","height":449, "thumbnail":"/webroot/elements/images/thumbs/content9.jpg"},
//         {"url":"/webroot/elements/content10.html","height":159, "thumbnail":"/webroot/elements/images/thumbs/content10.jpg", "sandbox":true},
//         {"url":"/webroot/elements/content11.html","height":89, "thumbnail":"/webroot/elements/images/thumbs/content11.jpg"},
//         {"url":"/webroot/elements/content12.html","height":300, "thumbnail":"/webroot/elements/images/thumbs/content12.jpg"},
//         {"url":"/webroot/elements/content13.html","height":366, "thumbnail":"/webroot/elements/images/thumbs/content13.jpg"},
//         {"url":"/webroot/elements/content14.html","height":512, "thumbnail":"/webroot/elements/images/thumbs/content14.jpg"},
//         {"url":"/webroot/elements/content15.html","height":453, "thumbnail":"/webroot/elements/images/thumbs/content15.jpg"},
//         {"url":"/webroot/elements/content16.html","height":299, "thumbnail":"/webroot/elements/images/thumbs/content16.jpg"},
//         {"url":"/webroot/elements/content17.html","height":531, "thumbnail":"/webroot/elements/images/thumbs/content17.jpg"},
//         {"url":"/webroot/elements/content18.html","height":605, "thumbnail":"/webroot/elements/images/thumbs/content18.jpg"},
//         {"url":"/webroot/elements/content19.html","height":702, "thumbnail":"/webroot/elements/images/thumbs/content19.jpg"},
//         {"url":"/webroot/elements/content20.html","height":339, "thumbnail":"/webroot/elements/images/thumbs/content20.jpg"},
//         {"url":"/webroot/elements/content21.html","height":410, "thumbnail":"/webroot/elements/images/thumbs/content21.jpg"},
//         {"url":"/webroot/elements/content22.html","height":366, "thumbnail":"/webroot/elements/images/thumbs/content22.jpg", "sandbox":true},
//         {"url":"/webroot/elements/content23.html","height":814, "thumbnail":"/webroot/elements/images/thumbs/content23.jpg"},
//         {"url":"/webroot/elements/content24.html","height":384, "thumbnail":"/webroot/elements/images/thumbs/content24.jpg"},
//         {"url":"/webroot/elements/content25.html","height":578, "thumbnail":"/webroot/elements/images/thumbs/content25.jpg"},
//         {"url":"/webroot/elements/content26.html","height":350, "thumbnail":"/webroot/elements/images/thumbs/content26.jpg"},
//         {"url":"/webroot/elements/content27.html","height":350, "thumbnail":"/webroot/elements/images/thumbs/content27.jpg"},
//         {"url":"/webroot/elements/content28.html","height":350, "thumbnail":"/webroot/elements/images/thumbs/content28.jpg"},
//         {"url":"/webroot/elements/content29.html","height":800, "thumbnail":"/webroot/elements/images/thumbs/content29.jpg"},
//         {"url":"/webroot/elements/content30.html","height":290, "thumbnail":"/webroot/elements/images/thumbs/content30.jpg"},
//         {"url":"/webroot/elements/content31.html","height":922, "thumbnail":"/webroot/elements/images/thumbs/content31.jpg"},
//         {"url":"/webroot/elements/content32.html","height":570, "thumbnail":"/webroot/elements/images/thumbs/content32.jpg", "sandbox":true},
//         {"url":"/webroot/elements/content33.html","height":578, "thumbnail":"/webroot/elements/images/thumbs/content33.jpg", "sandbox":true},
//         {"url":"/webroot/elements/content34.html","height":985, "thumbnail":"/webroot/elements/images/thumbs/content34.jpg"}
//     ],
//     "Services":[
//         {"url":"/webroot/elements/services1.html","height":542, "thumbnail":"/webroot/elements/images/thumbs/services1.jpg"},
//         {"url":"/webroot/elements/services2.html","height":692, "thumbnail":"/webroot/elements/images/thumbs/services2.jpg"},
//         {"url":"/webroot/elements/services3.html","height":310, "thumbnail":"/webroot/elements/images/thumbs/services3.jpg"},
//         {"url":"/webroot/elements/services6.html","height":310, "thumbnail":"/webroot/elements/images/thumbs/services6.jpg"},
//         {"url":"/webroot/elements/services9.html","height":450, "thumbnail":"/webroot/elements/images/thumbs/services9.jpg"},
//         {"url":"/webroot/elements/services4.html","height":551, "thumbnail":"/webroot/elements/images/thumbs/services4.jpg"},
//         {"url":"/webroot/elements/services8.html","height":354, "thumbnail":"/webroot/elements/images/thumbs/services8.jpg"},
//         {"url":"/webroot/elements/services5.html","height":300, "thumbnail":"/webroot/elements/images/thumbs/services5.jpg"},
//         {"url":"/webroot/elements/services7.html","height":310, "thumbnail":"/webroot/elements/images/thumbs/services7.jpg"}
//     ],
//     "Portfolios":[
//         {"url":"/webroot/elements/portfolio1.html","height":1372, "thumbnail":"/webroot/elements/images/thumbs/portfolio1.jpg"},
//         {"url":"/webroot/elements/portfolio2.html","height":939, "thumbnail":"/webroot/elements/images/thumbs/portfolio2.jpg"},
//         {"url":"/webroot/elements/portfolio3.html","height":2820, "thumbnail":"/webroot/elements/images/thumbs/portfolio3.jpg"}
//     ],
//     "Skills":[
//         {"url":"/webroot/elements/skills1.html","height":390, "thumbnail":"/webroot/elements/images/thumbs/skills1.jpg"},
//         {"url":"/webroot/elements/skills2.html","height":342, "thumbnail":"/webroot/elements/images/thumbs/skills2.jpg"},
//         {"url":"/webroot/elements/skills3.html","height":342, "thumbnail":"/webroot/elements/images/thumbs/skills3.jpg"}
//     ],
//     "Pricing Tables":[
//         {"url":"/webroot/elements/pricing1.html","height":550, "thumbnail":"/webroot/elements/images/thumbs/pricing1.jpg"},
//         {"url":"/webroot/elements/pricing2.html","height":550, "thumbnail":"/webroot/elements/images/thumbs/pricing2.jpg"},
//         {"url":"/webroot/elements/pricing3.html","height":350, "thumbnail":"/webroot/elements/images/thumbs/pricing3.jpg"},
//         {"url":"/webroot/elements/pricing4.html","height":351, "thumbnail":"/webroot/elements/images/thumbs/pricing4.jpg"},
//         {"url":"/webroot/elements/pricing5.html","height":500, "thumbnail":"/webroot/elements/images/thumbs/pricing5.jpg"},
//         {"url":"/webroot/elements/pricing6.html","height":464, "thumbnail":"/webroot/elements/images/thumbs/pricing6.jpg"},
//         {"url":"/webroot/elements/pricing7.html","height":460, "thumbnail":"/webroot/elements/images/thumbs/pricing7.jpg"}
//     ],
//     "Titles":[
//         {"url":"/webroot/elements/title1.html","height":156, "thumbnail":"/webroot/elements/images/thumbs/title1.jpg"},
//         {"url":"/webroot/elements/title2.html","height":156, "thumbnail":"/webroot/elements/images/thumbs/title2.jpg"},
//         {"url":"/webroot/elements/title3.html","height":156, "thumbnail":"/webroot/elements/images/thumbs/title3.jpg"},
//         {"url":"/webroot/elements/title4.html","height":144, "thumbnail":"/webroot/elements/images/thumbs/title4.jpg"},
//         {"url":"/webroot/elements/title5.html","height":134, "thumbnail":"/webroot/elements/images/thumbs/title5.jpg"},
//         {"url":"/webroot/elements/title6.html","height":132, "thumbnail":"/webroot/elements/images/thumbs/title6.jpg"},
//         {"url":"/webroot/elements/title7.html","height":132, "thumbnail":"/webroot/elements/images/thumbs/title7.jpg"}
//     ],
//     "Blog":[
//         {"url":"/webroot/elements/blog1.html","height":709, "thumbnail":"/webroot/elements/images/thumbs/blog1.jpg"},
//         {"url":"/webroot/elements/blog2.html","height":2761, "thumbnail":"/webroot/elements/images/thumbs/blog2.jpg"},
//         {"url":"/webroot/elements/blog3.html","height":2761, "thumbnail":"/webroot/elements/images/thumbs/blog3.jpg"},
//         {"url":"/webroot/elements/blog4.html","height":2761, "thumbnail":"/webroot/elements/images/thumbs/blog4.jpg"},
//         {"url":"/webroot/elements/blog5.html","height":2761, "thumbnail":"/webroot/elements/images/thumbs/blog5.jpg"}
//     ],
//     "Team":[
//         {"url":"/webroot/elements/team1.html","height":420, "thumbnail":"/webroot/elements/images/thumbs/team1.jpg"},
//         {"url":"/webroot/elements/team2.html","height":359, "thumbnail":"/webroot/elements/images/thumbs/team2.jpg"},
//         {"url":"/webroot/elements/team3.html","height":459, "thumbnail":"/webroot/elements/images/thumbs/team3.jpg"},
//         {"url":"/webroot/elements/team4.html","height":491, "thumbnail":"/webroot/elements/images/thumbs/team4.jpg"}
//     ],
//     "Basic Blocks":[
//         {"url":"/webroot/elements/basic1.html","height":273, "thumbnail":"/webroot/elements/images/thumbs/basic1.jpg"},
//         {"url":"/webroot/elements/basic2.html","height":221, "thumbnail":"/webroot/elements/images/thumbs/basic2.jpg"},
//         {"url":"/webroot/elements/basic3.html","height":261, "thumbnail":"/webroot/elements/images/thumbs/basic3.jpg"},
//         {"url":"/webroot/elements/basic4.html","height":270, "thumbnail":"/webroot/elements/images/thumbs/basic4.jpg"},
//         {"url":"/webroot/elements/basic5.html","height":270, "thumbnail":"/webroot/elements/images/thumbs/basic5.jpg"},
//         {"url":"/webroot/elements/basic6.html","height":270, "thumbnail":"/webroot/elements/images/thumbs/basic6.jpg"},
//         {"url":"/webroot/elements/basic7.html","height":209, "thumbnail":"/webroot/elements/images/thumbs/basic7.jpg"},
//         {"url":"/webroot/elements/basic8.html","height":219, "thumbnail":"/webroot/elements/images/thumbs/basic8.jpg"},
//         {"url":"/webroot/elements/basic9.html","height":254, "thumbnail":"/webroot/elements/images/thumbs/basic9.jpg"},
//         {"url":"/webroot/elements/basic10.html","height":275, "thumbnail":"/webroot/elements/images/thumbs/basic10.jpg"},
//         {"url":"/webroot/elements/basic11.html","height":275, "thumbnail":"/webroot/elements/images/thumbs/basic11.jpg"},
//         {"url":"/webroot/elements/basic12.html","height":276, "thumbnail":"/webroot/elements/images/thumbs/basic12.jpg"},
//         {"url":"/webroot/elements/basic13.html","height":209, "thumbnail":"/webroot/elements/images/thumbs/basic13.jpg"},
//         {"url":"/webroot/elements/basic14.html","height":218, "thumbnail":"/webroot/elements/images/thumbs/basic14.jpg"},
//         {"url":"/webroot/elements/basic15.html","height":252, "thumbnail":"/webroot/elements/images/thumbs/basic15.jpg"},
//         {"url":"/webroot/elements/basic16.html","height":275, "thumbnail":"/webroot/elements/images/thumbs/basic16.jpg"},
//         {"url":"/webroot/elements/basic17.html","height":273, "thumbnail":"/webroot/elements/images/thumbs/basic17.jpg"},
//         {"url":"/webroot/elements/basic18.html","height":273, "thumbnail":"/webroot/elements/images/thumbs/basic18.jpg"}
//     ],
//     "Downloads":[
//         {"url":"/webroot/elements/download1.html","height":227, "thumbnail":"/webroot/elements/images/thumbs/download1.jpg"},
//         {"url":"/webroot/elements/download2.html","height":468, "thumbnail":"/webroot/elements/images/thumbs/download2.jpg"},
//         {"url":"/webroot/elements/download3.html","height":408, "thumbnail":"/webroot/elements/images/thumbs/download3.jpg"},
//         {"url":"/webroot/elements/download4.html","height":379, "thumbnail":"/webroot/elements/images/thumbs/download4.jpg"},
//         {"url":"/webroot/elements/download5.html","height":629, "thumbnail":"/webroot/elements/images/thumbs/download5.jpg"}
//     ],
//     "Dividers":[
//         {"url":"/webroot/elements/divider1.html","height":39, "thumbnail":"/webroot/elements/images/thumbs/divider1.jpg"},
//         {"url":"/webroot/elements/divider2.html","height":39, "thumbnail":"/webroot/elements/images/thumbs/divider2.jpg"},
//         {"url":"/webroot/elements/divider3.html","height":39, "thumbnail":"/webroot/elements/images/thumbs/divider3.jpg"},
//         {"url":"/webroot/elements/divider4.html","height":39, "thumbnail":"/webroot/elements/images/thumbs/divider4.jpg"}
//     ],
//     "Extra Pages":[
//         {"url":"/webroot/elements/page404_1.html","height":667, "thumbnail":"/webroot/elements/images/thumbs/page-404-1.jpg"},
//         {"url":"/webroot/elements/pagelogin1.html","height":667, "thumbnail":"/webroot/elements/images/thumbs/pagelogin1.jpg"},
//         {"url":"/webroot/elements/pageregister1.html","height":449, "thumbnail":"/webroot/elements/images/thumbs/pageregister1.jpg"},
//         {"url":"/webroot/elements/pageloginregister1.html","height":410, "thumbnail":"/webroot/elements/images/thumbs/pageloginregister1.jpg"}
//     ],
//     "Contact":[
//         {"url":"/webroot/elements/contact1.html","height":500, "thumbnail":"/webroot/elements/images/thumbs/contact1.jpg"},
//         {"url":"/webroot/elements/contact2.html","height":561, "thumbnail":"/webroot/elements/images/thumbs/contact2.jpg"},
//         {"url":"/webroot/elements/contact3.html","height":545, "thumbnail":"/webroot/elements/images/thumbs/contact3.jpg"}
//     ],
//     "Footers":[
//         {"url":"/webroot/elements/footer1.html","height":447, "thumbnail":"/webroot/elements/images/thumbs/footer1.jpg"},
//         {"url":"/webroot/elements/footer2.html","height":78, "thumbnail":"/webroot/elements/images/thumbs/footer2.jpg"},
//         {"url":"/webroot/elements/footer3.html","height":54, "thumbnail":"/webroot/elements/images/thumbs/footer3.jpg"},
//         {"url":"/webroot/elements/footer4.html","height":395, "thumbnail":"/webroot/elements/images/thumbs/footer4.jpg"},
//         {"url":"/webroot/elements/footer5.html","height":446, "thumbnail":"/webroot/elements/images/thumbs/footer5.jpg"},
//         {"url":"/webroot/elements/footer6.html","height":254, "thumbnail":"/webroot/elements/images/thumbs/footer6.jpg"},
//         {"url":"/webroot/elements/footer7.html","height":252, "thumbnail":"/webroot/elements/images/thumbs/footer7.jpg"},
//         {"url":"/webroot/elements/footer8.html","height":144, "thumbnail":"/webroot/elements/images/thumbs/footer8.jpg"}
//     ]
//     }
// }
 ?>