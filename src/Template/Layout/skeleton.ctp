<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <title>Basic</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Loading Bootstrap -->
    <?= $this->Html->css('elements/css/bootstrap.css') ?>
    
    <!-- Loading General Styles -->    
    <?= $this->Html->css('elements/css/style-services.css') ?>
    <?= $this->Html->css('elements/css/style-navigation.css') ?>
    <?= $this->Html->css('elements/css/style-headers.css') ?>
    <?= $this->Html->css('elements/css/style-content.css') ?>
    <?= $this->Html->css('elements/css/style-extra-pages.css') ?>
    <?= $this->Html->css('elements/css/style-basic.css') ?>
    <?= $this->Html->css('elements/css/style-team.css') ?>
    <?= $this->Html->css('elements/css/style-intro.css') ?>
    <?= $this->Html->css('elements/css/style-divider.css') ?>
    <?= $this->Html->css('elements/css/style-download.css') ?>
    <?= $this->Html->css('elements/css/font-awesome.min.css') ?>
    <?= $this->Html->css('elements/css/owl.carousel.css') ?>
        
    <link rel="shortcut icon" href="/img/icon.png">
    
    <!-- Font -->
   	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800' rel='stylesheet' type='text/css'>
   	<link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <?= $this->Html->script('elements/js/html5shiv.js'); ?>
    <?= $this->Html->script('elements/js/respond.min.js'); ?>
    <![endif]-->
</head>

<body>

    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>



    <!-- Important js put in all pages -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


    <!-- Load JS -->
    <?= $this->Html->script('elements/js/jquery-1.9.1.min.js'); ?>
    <?= $this->Html->script('elements/js/bootstrap.min.js'); ?>
    
    <?= $this->Html->script('elements/js/owl.carousel.min.js'); ?>
    <?= $this->Html->script('elements/js/jquery.easy-pie-chart.js'); ?>
    <?= $this->Html->script('elements/js/main.js'); ?>
    
</body>
</html>
