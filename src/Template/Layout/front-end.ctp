<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Sesniel</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Senior Full Stack Developer" />
	<meta name="keywords" content="PHP Web Developer" />
	<meta name="author" content="Joshua Sesniel Deloso" />


	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<!-- Animate.css -->
        
        <?= $this->Html->css('front-end/animate.css') ?>
        <?= $this->Html->css('front-end/simple-line-icons.css') ?>
        <?= $this->Html->css('front-end/magnific-popup.css') ?>
        <?= $this->Html->css('front-end/bootstrap.css') ?>
        <?= $this->Html->css('front-end/style.css') ?>



	<!-- Modernizr JS -->
        <?= $this->Html->script('front-end/modernizr-2.6.2.min.js'); ?>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	<style>
	.icon-color{color: #87cab4;}

	.padd-space{ padding:20px; }
	
	.contact-background{
	    background: white;
	    padding: 30px;
	    border-radius: 25px;
	}
        
        .map{
            position:relative;
            top:150px;
        }

	</style>
	</head>
	<body>


        <?= $this->fetch('content') ?>


        <footer role="contentinfo">
            <div class="navbar navbar-default">        
                <p class="navbar-text pull-left">2017 © SESNIEL<sup>Passionate Developer</sup> </p>
                <div class="pull-right"> 
                    <ul class="social social-circle1">
                            <li><a href="https://www.linkedin.com/in/joshua-sesniel-d-b1802b32/" target="_blank"><i class="fa fa-linkedin-square fa-2x padd-space" aria-hidden="true"></i></a></li>
                            <li><a href="https://github.com/sesniel" target="_blank"><i class="fa fa-github-square fa-2x padd-space" aria-hidden="true"></i></a></li>

                    </ul>
                </div>
            </div>
        </footer>

        <?= $this->Html->script('front-end/jquery.min.js'); ?>
        <?= $this->Html->script('front-end/jquery.easing.1.3.js'); ?>
        <?= $this->Html->script('front-end/bootstrap.min.js'); ?>
        <?= $this->Html->script('front-end/jquery.waypoints.min.js'); ?>
        <?= $this->Html->script('front-end/jquery.stellar.min.js'); ?>
        <?= $this->Html->script('front-end/jquery.countTo.js'); ?>
        <?= $this->Html->script('front-end/jquery.magnific-popup.min.js'); ?>
        <?= $this->Html->script('front-end/magnific-popup-options.js'); ?>
	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
	
        <?= $this->Html->script('front-end/google_map.js'); ?>
        <?= $this->Html->script('front-end/main.js'); ?>
        

	</body>
</html>

