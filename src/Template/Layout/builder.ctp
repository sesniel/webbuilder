<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <title>Basic Block</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="shortcut icon" href="/img/icon.png">
    <!-- Loading Bootstrap -->
    <?= $this->Html->css('vendor/bootstrap.min.css') ?>
    <?= $this->Html->css('flat-ui-pro.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('font-awesome.css') ?>
    <?= $this->Html->css('builder.css') ?>
    <?= $this->Html->css('spectrum.css') ?>
    <?= $this->Html->css('chosen.css') ?>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        
    <link rel="shortcut icon" href="/webroot/images/favicon.ico">
    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <?= $this->Html->script('html5shiv.js'); ?>
    <?= $this->Html->script('respond.min.js'); ?>
    <![endif]-->
    <!--[if lt IE 10]>
    <?= $this->Html->css('ie-masonry.css') ?>
    <?= $this->Html->script('masonry.pkgd.min.js'); ?>
    <![endif]--> 
        
    
</head>

<body>

    <?= $this->fetch('content') ?>




    <!-- Load JS -->
    <?= $this->Html->script('vendor/jquery.min.js'); ?>
    <?= $this->Html->script('vendor/jquery-ui.min.js'); ?>
    <?= $this->Html->script('vendor/flat-ui-pro.min.js'); ?>
    <?= $this->Html->script('vendor/chosen.min.js'); ?>
    <?= $this->Html->script('vendor/jquery.zoomer.js'); ?>
    <?= $this->Html->script('vendor/spectrum.js'); ?>
    <?= $this->Html->script('vendor/ace/ace.js'); ?>
    <?= $this->Html->script('builder.js'); ?>
    
</body>
</html>
