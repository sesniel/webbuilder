<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <title>Basic Block</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="shortcut icon" href="/img/icon.png">
    <!-- Loading Bootstrap -->
    <?= $this->Html->css('vendor/bootstrap.min.css') ?>
    <?= $this->Html->css('flat-ui-pro.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('font-awesome.css') ?>
    <?= $this->Html->css('builder.css') ?>
    <?= $this->Html->css('spectrum.css') ?>
    <?= $this->Html->css('chosen.css') ?>
    
    <?= $this->Html->css('elements/css/style-services.css') ?>
    <?= $this->Html->css('elements/css/style-navigation.css') ?>
    <?= $this->Html->css('elements/css/style-headers.css') ?>
    <?= $this->Html->css('elements/css/style-content.css') ?>
    <?= $this->Html->css('elements/css/style-extra-pages.css') ?>
    <?= $this->Html->css('elements/css/style-basic.css') ?>
    <?= $this->Html->css('elements/css/style-team.css') ?>
    <?= $this->Html->css('elements/css/style-intro.css') ?>
    <?= $this->Html->css('elements/css/style-divider.css') ?>
    <?= $this->Html->css('elements/css/style-download.css') ?>
    <?= $this->Html->css('elements/css/owl.carousel.css') ?>
        
    <link rel="shortcut icon" href="/img/icon.png">
    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <?= $this->Html->script('html5shiv.js'); ?>
    <?= $this->Html->script('respond.min.js'); ?>
    <![endif]-->
    <!--[if lt IE 10]>
    <?= $this->Html->css('ie-masonry.css') ?>
    <?= $this->Html->script('masonry.pkgd.min.js'); ?>
    <![endif]--> 
        
    
    <!-- Font -->
   	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800' rel='stylesheet' type='text/css'>
   	<link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400' rel='stylesheet' type='text/css'>

</head>

<body>

    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>



    <!-- Important js put in all pages -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <!-- Subpages JS -->
    <?= $this->Html->script('elements/js/jquery-1.9.1.min.js'); ?>
    <?= $this->Html->script('elements/js/bootstrap.min.js'); ?>
    
    <?= $this->Html->script('elements/js/owl.carousel.min.js'); ?>
    <?= $this->Html->script('elements/js/jquery.easy-pie-chart.js'); ?>
    <?= $this->Html->script('elements/js/main.js'); ?>

    <!-- Load JS -->
    <?= $this->Html->script('vendor/jquery.min.js'); ?>
    <?= $this->Html->script('vendor/jquery-ui.min.js'); ?>
    <?= $this->Html->script('vendor/flat-ui-pro.min.js'); ?>
    <?= $this->Html->script('vendor/chosen.min.js'); ?>
    <?= $this->Html->script('vendor/jquery.zoomer.js'); ?>
    <?= $this->Html->script('vendor/spectrum.js'); ?>
    <?= $this->Html->script('vendor/ace/ace.js'); ?>
    
    <?= $this->Html->script('builder.js'); ?>
    
    
</body>
</html>
