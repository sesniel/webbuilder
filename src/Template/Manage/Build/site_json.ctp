<?php

$val = count($pagesDetails->toArray()) - 1; 
        
$display = '{"pages":{';

    foreach($pagesDetails as $key => $page): 
    
        if($val == $key):             
            $display .= '"'.$page->name.'":{ "blocks":' . htmlspecialchars(json_encode(json_decode($page->code))) . '}';
        else:
            $display .= '"'.$page->name.'":{ "blocks":' . htmlspecialchars(json_encode(json_decode($page->code))) . '},';            
        endif;
        
        
    endforeach;

$display .= '},"responsiveMode":"tablet"}';

echo $display;

?>