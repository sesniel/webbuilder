<header role="banner" id="fh5co-header">
    <div class="container">
                <!-- <div class="row"> -->
            <nav class="navbar navbar-default">
        <div class="navbar-header">
                <!-- Mobile Toggle Menu Button -->
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
         <a class="navbar-brand" href="index.html">Sesniel</a> 
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="#" data-nav-section="home"><span>Home</span></a></li>
            <li><a href="#" data-nav-section="about"><span>About</span></a></li>
            <li><a href="#" data-nav-section="projects"><span>Projects</span></a></li>
            <li><a href="#" data-nav-section="contact"><span>Contact</span></a></li>
          </ul>
        </div>
            </nav>
          <!-- </div> -->
    </div>
</header>

<section id="fh5co-home" data-section="home" data-stellar-background-ratio="0.5">
        <div class="gradient"></div>
        <div class="container">
                <div class="text-wrap">
                        <div class="text-inner">
                                <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                                <img class="to-animate" src="images/sesniel.png" alt="Image" style="width: 100%;">
                                                <h2 class="to-animate">Know me more!</h2>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
        <div class="slant"></div>
</section>

<section id="fh5co-intro" data-section="about">
        <div class="container">
                <div class="row row-bottom-padded-lg" style="position: relative; top: 200px;">
                        <div class="row">
                                <div class="col-md-12 section-heading text-center">
                                        <h2 class="to-animate">About</h2>
                                        <div class="row">
                                                <div class="col-md-8 col-md-offset-2 subtext to-animate">
                                                        <h3>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</h3>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="fh5co-block to-animate">
                                <div class="overlay-darker"></div>
                                <div class="overlay"></div>
                                <div class="fh5co-text">
                                        <i class="fa fa-list-alt fa-5x icon-color" aria-hidden="true"></i>
                                        <h2>Resume</h2>
                                        <p>Credentials are papers, i believe that true education comes in attitude toward work, and respect to others.</p>
                                        <p><a href="#" class="btn btn-primary">Access Page</a></p>
                                </div>
                        </div>
                        <div class="fh5co-block to-animate">
                                <div class="overlay-darker"></div>
                                <div class="overlay"></div>
                                <div class="fh5co-text">
                                        <i class="fa fa-desktop fa-5x icon-color" aria-hidden="true"></i>
                                        <h2>Experience</h2>
                                        <p>Since i was a kid, i've been enthusiastic in computers, my years of experience has guided and made me informed. </p>
                                        <p><a href="#" class="btn btn-primary">Access Page</a></p>
                                </div>
                        </div>
                        <div class="fh5co-block to-animate">
                                <div class="overlay-darker"></div>
                                <div class="overlay"></div>
                                <div class="fh5co-text">
                                        <i class="fa fa-motorcycle fa-5x icon-color" aria-hidden="true"></i>
                                        <h2>Hobbies</h2>
                                        <p>Working is very important, but a balance life is necessary, here are my simple remedies in life that helps me.</p>
                                        <p><a href="#" class="btn btn-primary">Access Page</a></p>
                                </div>
                        </div>
                </div>
        </div>
</section>

<section id="fh5co-work" data-section="projects">
        <div class="container">
                <div class="row">
                        <div class="col-md-12 section-heading text-center">
                                <h2 class="to-animate">Projects</h2>
                        </div>
                </div><div class="row row-bottom-padded-sm">
				<div class="col-md-4 col-sm-6 col-xxs-12 ">
                                    <div class="fh5co-project-item">
					<a href="#" class=" image-popup to-animate fadeInUp animated">
                                            <?php echo $this->Html->image('front-end/work_1.jpg', ['class' => 'img-responsive']); ?>
					</a>
                                        <div class="fh5co-text">
                                            <h2>Web Builder</h2>
                                            <span>Personal</span>
                                        </div>
                                    </div>
				</div>
				<div class="col-md-4 col-sm-6 col-xxs-12 ">
                                    <div class="fh5co-project-item">
					<a href="#" class=" image-popup to-animate fadeInUp animated">
                                            <?php echo $this->Html->image('front-end/work_1.jpg', ['class' => 'img-responsive']); ?>
					</a>
                                        <div class="fh5co-text">
                                            <h2>Project 1</h2>
                                            <span>Branding</span>
                                        </div>
                                    </div>
				</div>
				<div class="col-md-4 col-sm-6 col-xxs-12 ">
                                    <div class="fh5co-project-item">
					<a href="#" class=" image-popup to-animate fadeInUp animated">
                                            <?php echo $this->Html->image('front-end/work_1.jpg', ['class' => 'img-responsive']); ?>
					</a>
                                        <div class="fh5co-text">
                                            <h2>Project 2</h2>
                                            <span>Branding</span>
                                        </div>
                                    </div>
				</div>
			</div>

                </div>
        </div>
</section>


<section id="fh5co-intro" class="map" data-section="map">
        <div class="container">                    
            <div id="map" class="to-animate"></div>
        </div>
</section>

<section id="fh5co-work" data-section="contact">
        <div class="container">
                <div class="row">
                        <div class="col-md-12 section-heading text-center">
                                <h2 class="to-animate">Get In Touch</h2>
                        </div>
                </div>
                <div class="row row-bottom-padded-md">
                        <div class="col-md-6 to-animate">
                                <h3>Contact Info</h3>
                                <ul class="fh5co-contact-info">
                                        <li class="fh5co-contact-address ">
                                            <i class="fa fa-home" aria-hidden="true"></i>
                                                Pulung Cacutud Angeles City, <br>Pampanga Philippines
                                        </li>
                                        <li><i class="fa fa-phone" aria-hidden="true"></i> (+63) 977-374-3741</li>
                                        <li><i class="fa fa-envelope" aria-hidden="true"></i> jsp.deloso@gmail.com</li>
                                </ul>
                        </div>
                    
                        <div class="col-md-6 to-animate contact-background">
                            <?= $this->Form->create(null, ['url' => ['action' => 'contact']]);  ?>
                                <h3>Contact Form</h3>
                                <div class="form-group ">
                                        <label for="name" class="sr-only">Name</label>
                                        <input name="name" class="form-control" placeholder="Name" type="text">
                                </div>
                                <div class="form-group ">
                                        <label for="email" class="sr-only">Email</label>
                                        <input name="email" class="form-control" placeholder="Email" type="email">
                                </div>
                                <div class="form-group ">
                                        <label for="phone" class="sr-only">Phone</label>
                                        <input name="phone" class="form-control" placeholder="Phone" type="text">
                                </div>
                                <div class="form-group ">
                                        <label for="message" class="sr-only">Message</label>
                                        <textarea name="message" id="message" cols="30" rows="5" class="form-control" placeholder="Message"></textarea>
                                </div>
                                <div class="form-group ">
                                        <input class="btn btn-primary btn-lg" value="Send Message" type="submit">
                                </div>
                            <?= $this->Form->end(); ?>
                        </div>
                        </div>

                </div>
        </div>
</section>



